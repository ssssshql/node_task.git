const $axios = require('../config/axios')
const $log=require('../config/logConfig.js')
const path = require('path');
const scriptName = path.basename(__filename);

var config = {
    method: 'get',
    url: 'https://api.dddh.pub/wx/addScore',
    params:{
        type:'WX_BANNER_AD'
    },
    headers: {
       'uid': '根据实际填', 
       'charset': 'utf-8', 
       'version': '6', 
       'User-Agent': 'apifox/1.0.0 (https://www.apifox.cn)', 
       'content-type': 'application/x-www-form-urlencoded'
    }
 };

 $axios(config)
 .then(function (res) {
    $log.info(scriptName+'----'+JSON.stringify(res));
 })
 .catch(function (error) {
    $log.error(scriptName+'----'+JSON.stringify(error.response.data));
 });